#Gal Turgeman 205568553, FINAL EXAM 

library(ggplot2)
library(dplyr)
library(caTools)
library(lubridate)
library(rpart)
library(rpart.plot)
library(corrplot)
library(randomForest)
library(pROC)
library(ROSE)
library(tm)

setwd("C:/Users/USER/Desktop/test")
#1.1 - data import 
article.raw <- read.csv('OnionOrNot.csv')
article <- article.raw
str (article)
summary(article)

#1.2 lable change Factor, 1=YES,0=NO
article$label <-as.factor(article$label)

ggplot(article,aes(label))+geom_bar()

make_YES.NO<- function(x){
  if (x == 1) 
    return('Yes')
  return('No') 
}

new <- sapply(article$label, make_YES.NO)
article$label <-new

str (article)
table(article$label)
#1.3 text change chr
article$text <-as.character(article$text)

#1.4 pie 

mytable <- table(article$label)
lbls <- paste(names(mytable), "\n", mytable, sep="")
pie(mytable, labels = lbls,
    main="Pie Chart")

#2.1
onion.corpus <- Corpus(VectorSource(article$text))
onion.corpus[[1]][[1]]
onion.corpus[[1]][[2]]
onion.corpus[[2]][[1]]

clean.corpus <- tm_map(onion.corpus, removePunctuation)
clean.corpus[[4]][[1]]

clean.corpus <- tm_map(clean.corpus, removeNumbers)

clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))

clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())

clean.corpus <- tm_map(clean.corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean.corpus)

dim(dtm)


#2.2
dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,10))) 

dim(dtm.freq)



#3.1
#Split to test and train 

filter <- sample.split(article$label,SplitRatio = 0.7 )

onion.train <- subset(article,filter==T)

onion.test <- subset(article,filter==F)

dim(onion.train)
dim(onion.test)

onion.model <- glm(label~ label, family = binomial(link = "logit"), data = onion.train)

summary(onion.model)

predition <- predict(onion.model, onion.test, type = 'response')

hist(predition)

actual <- onion.test$label

confusion_matrix <- table(actual,predition > 0.5)

precision <- confusion_matrix[1,1]/(confusion_matrix[1,1] + confusion_matrix[1,1])
recall <- confusion_matrix[1,1]/(confusion_matrix[1,1] + confusion_matrix[2,1]) 


####3.2
filter <- sample.split(article$label, SplitRatio = 0.7)
onion1.train <- subset(article, filter == T)
onion1.test <- subset(article, filter == F)


model.dt <- rpart(label ~ label, onion1.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction <- predict(model.dt,onion1.test)
actual <- onion1.test$label

cf <- table(actual,prediction > 0.5)
precision <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['TRUE','FALSE'])
recall <- cf['TRUE','TRUE']/(cf['TRUE','TRUE'] + cf['FALSE','TRUE'])

model.dt <- rpart(text ~ label, onion1.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)
